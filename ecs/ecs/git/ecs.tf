provider "aws" {
  region = "us-east-2"
}

module "ecs" {
  source = "modules/ecs"

  environment          = "${var.environment}"
  cluster              = "${var.environment}"
  cloudwatch_prefix    = "${var.environment}"        
  vpc_cidr             = "${var.vpc_cidr}"
  public_subnet_cidrs  = "${var.public_subnet_cidrs}"
  private_subnet_cidrs = "${var.private_subnet_cidrs}"
  availability_zones   = "${var.availability_zones}"
  max_size             = "${var.max_size}"
  min_size             = "${var.min_size}"
  desired_capacity     = "${var.desired_capacity}"
  key_name             = "${aws_key_pair.ecs.key_name}"
  instance_type        = "${var.instance_type}"
  ecs_aws_ami          = "${var.ecs_aws_ami}"
}
resource "aws_key_pair" "ecs" {
  key_name   = "ecs-key-${var.environment}"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDZ/XI5WD5q6HPj4r+x2ArPfj5iBGpKJ2VuwZlWGJ51vwAeFnLJMKnWzzs8mCuj3ybuDMU/T3nk+u6n//hWmtu8grDQXw2Tt+mWF+z1cVTXStqlTDzkUrAbCidZ6OHqaOM0qS83aSSbntatQ3OlYzSZgBE8oW0tzEMP7sqJ0Ou2wlx2oCr/adhrdMkhpxw5H2ORMKDTzmYGxhkksE/1/5qdkgBsBJwSjvx2lIHcKCakEs4SeHhlCpXb7CjxdHzW0NB/SucjoLiRGD9hEMAtnDvTOeMpjsCkwQ835KKnryRsL3Wi0tb6N67zbVOguT85ySZ0o3O5ZwhjaflPFZZqiKcJ root@ip-172-31-22-48.us-east-2.compute.internal"
}

variable "vpc_cidr" {}
variable "environment" {}
variable "max_size" {}
variable "min_size" {}
variable "desired_capacity" {}
variable "instance_type" {}
variable "ecs_aws_ami" {}

variable "private_subnet_cidrs" {
  type = "list"
}

variable "public_subnet_cidrs" {
  type = "list"
}

variable "availability_zones" {
  type = "list"
}

output "default_alb_target_group" {
  value = "${module.ecs.default_alb_target_group}"
}
