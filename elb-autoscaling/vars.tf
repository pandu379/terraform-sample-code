variable "AWS_REGION" {
  default = "us-east-2"
}
variable "ecs-autoscaling" {
  description = "auto scaling name"
}
variable "my-elb" {
  description = " elb name "
}
variable "AMIS" {
  type = "map"
  default = {
    us-east-2 = "ami-0c55b159cbfafe1f0"
    us-west-2 = "ami-06b94666"
    eu-west-1 = "ami-844e0bf7"
  }
}
