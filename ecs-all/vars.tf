variable "AWS_REGION" {
  default = "us-east-2"
}
variable "ECS_INSTANCE_TYPE" {
  default = "t2.micro"
}
variable "ecs-autoscaling" {
  description = " auto scaling name"
}
variable "my-elb" {
  description = " elb name"
}
variable "ecs-cluster" {
  description = " cluster name"
}
variable "ecr-repo" {
  description = " ecr repo name "
}
variable "my-service" {
  description = " service name"
}
#variable "max_instance_size" {
#  description = "Maximum number of instances in the cluster"
#}
#variable "min_instance_size" {
#  description = "Minimum number of instances in the cluster"
#}
variable "my-elb-securitygroup" {
  default = " sg-67949f09 "
}
variable "ECS_AMIS" {
  type = "map"
  default = {
    us-east-2 = "ami-0c55b159cbfafe1f0"
    us-west-2 = "ami-56ed4936"
    eu-west-1 = "ami-c8337dbb"
  }
}
