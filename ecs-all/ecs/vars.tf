variable "AWS_REGION" {
  default = "us-east-2"
}
variable "aws_access_key_id" {
  description = "AWS access key"
}
variable "aws_secret_access_key" {
  description = "AWS secret access key"
}
variable "ECS_INSTANCE_TYPE" {
  default = "t2.micro"
}
variable "ecs_cluster" {
  description = "ECS cluster name"
}
variable "ecr_repo" {
  description = "ecr repo name"
}
variable "min_instance_size" {
  default = "4"
  description = "Minimum number of instances in the cluster"
}
variable "max_instance_size" {
  default = "10"
  description = "Maximum number of instances in the cluster"
}
variable "ECS_AMIS" {
  type = "map"
  default = {
    us-east-2 = "ami-0c55b159cbfafe1f0"
    us-west-2 = "ami-56ed4936"
    eu-west-1 = "ami-c8337dbb"
  }
}

variable "ecs-autoscaling" {
  description = " auto scaling name"
}
