#resource "aws_elb" "my-elb" {
#  name = "${var.my-elb}"

# listener {
#  instance_port = 4567
# instance_protocol = "http"
#lb_port = 80
#lb_protocol = "http"
# }

#health_check {
#  healthy_threshold = 3
#  unhealthy_threshold = 3
#  timeout = 30
#  target = "HTTP:4567/"
#  interval = 60
# }

# cross_zone_load_balancing = true
# idle_timeout = 400
# connection_draining = true
# connection_draining_timeout = 400

# subnets = ["${aws_subnet.main-public-1.id}","${aws_subnet.main-public-2.id}"]
#  security_groups = ["${aws_security_group.my-elb-securitygroup.id}"]
#  security_groups = ["${self.id}"]
  
#  load_balancer {
# elb_name = "${aws_elb.my-elb.name}"
# container_name = "myapp"
# container_port = 4567
# }
#}







resource "aws_alb_target_group" "test" {
name = "my-alb-group"
port = 80
protocol = "HTTP"
vpc_id = "${module.main.vpc_id}"
}

resource "aws_alb" "main" {
name = "my-alb-ecs"
subnets = ["${module.main-vpc.public_subnets}"]
security_groups = ["${module.main-vpc.default_security_group_id}"]
}

resource "aws_alb_listener" "front_end" {
load_balancer_arn = "${aws_alb.main.id}"
port = "80"
protocol = "HTTP"

default_action {
target_group_arn = "${aws_alb_target_group.test.id}"
type = "forward"
}
}
