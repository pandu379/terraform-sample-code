variable "vpc-cidr"{
  default = "180.10.0.0/16"
}
variable "subnet-cidr"{
  type = "list"
  default =[ "180.10.1.0/24","180.10.2.0/24"]
}
variable "azs"{
  type = "list"
  default = ["us-east-2a","us-east-2b"]
}
